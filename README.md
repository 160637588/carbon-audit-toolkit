# Carbon audit toolkit

This is a toolkit for carbon auditing of a building in Hong Kong. This repository is the frontend and backend of the toolkit.

# Requirement

* PHP 7.0+
* Java JDK 1.8 (Oracle only, openJDK does not work)

# Installation

1. Copy `/form/config/config.sample.php` to `/form/config/config.php`
2. Change `config.php` accordingly
3. Create the MySQL Database from `ghg_database_no_data.sql`. Note: No data is included due to license issues

# Thesis

[DEVELOPMENT OF TOOLKITS AND DATABASES FOR CARBON AUDIT](https://www.researchgate.net/publication/341726003_DEVELOPMENT_OF_TOOLKITS_AND_DATABASES_FOR_CARBON_AUDIT)

Author: SHAM Koon Yu

Year: 2020

DOI: 10.13140/RG.2.2.34698.90566/1

# Acknowledgment 
This toolkit is based on the following software.

*	[PHP-MySQLi-Database-Class](https://github.com/ThingEngineer/PHP-MySQLi-Database-Class)
    Copyright (C) 2013  Josh Campbell (ajillion)
    GPLv3
*	[Bootstrap](https://github.com/twbs/bootstrap)
    Copyright (c) 2011-2019 Twitter, Inc.
    Copyright (c) 2011-2019 The Bootstrap Authors
    MIT License
* [bootstrap4c-chosen](https://github.com/haubek/bootstrap4c-chosen)
    Copyright (c) 2017-19 Martin Haubek
    MIT License
* [phpjasper](https://github.com/PHPJasper/phpjasper)
    Copyright (c) 2013 Hélder Duarte
    Copyright (c) 2015 Daniel Rodrigues Lima (geekcom)
    Copyright (c) 2015 Leandro Bitencourt
    MIT License