-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Linux (x86_64)
--
-- Host: 192.168.1.199    Database: ghg
-- ------------------------------------------------------
-- Server version	10.4.12-MariaDB-1:10.4.12+maria~buster-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `section1`
--

DROP TABLE IF EXISTS `section1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section1` (
  `audit_id` int(11) NOT NULL AUTO_INCREMENT,
  `building_name` varchar(255) NOT NULL,
  `building_stage` varchar(255) NOT NULL,
  `building_location` mediumtext NOT NULL,
  `property_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`audit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `section2`
--

DROP TABLE IF EXISTS `section2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audit_id` int(11) NOT NULL,
  `activity` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `quantity` double NOT NULL,
  `unit` varchar(16) NOT NULL,
  `factor` double NOT NULL,
  `emission` double GENERATED ALWAYS AS (`factor` * `quantity`) STORED,
  PRIMARY KEY (`id`,`audit_id`),
  KEY `activity_fkey_sect2` (`activity`),
  KEY `audit_id_fkey_sect2` (`audit_id`),
  CONSTRAINT `activity_fkey_sect2` FOREIGN KEY (`activity`) REFERENCES `emission_factor` (`activity`),
  CONSTRAINT `audit_id_fkey_sect2` FOREIGN KEY (`audit_id`) REFERENCES `section1` (`audit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `section3`
--

DROP TABLE IF EXISTS `section3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audit_id` int(11) NOT NULL,
  `activity` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `quantity` double NOT NULL,
  `unit` varchar(16) NOT NULL,
  `factor` double NOT NULL,
  `emission` double GENERATED ALWAYS AS (`factor` * `quantity`) STORED,
  PRIMARY KEY (`id`,`audit_id`),
  KEY `activity_fkey` (`activity`),
  KEY `audit_id_fkey` (`audit_id`),
  CONSTRAINT `activity_fkey_sect3` FOREIGN KEY (`activity`) REFERENCES `emission_factor` (`activity`),
  CONSTRAINT `audit_id_fkey_sect3` FOREIGN KEY (`audit_id`) REFERENCES `section1` (`audit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `section4`
--

DROP TABLE IF EXISTS `section4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audit_id` int(11) NOT NULL,
  `activity` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `quantity` double NOT NULL,
  `unit` varchar(16) NOT NULL,
  `factor` double NOT NULL,
  `emission` double GENERATED ALWAYS AS (`factor` * `quantity`) STORED,
  PRIMARY KEY (`id`,`audit_id`),
  KEY `activity_fkey` (`activity`),
  KEY `audit_id_fkey` (`audit_id`),
  CONSTRAINT `activity_fkey_sect4` FOREIGN KEY (`activity`) REFERENCES `emission_factor` (`activity`),
  CONSTRAINT `audit_id_fkey_sect4` FOREIGN KEY (`audit_id`) REFERENCES `section1` (`audit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emission_factor`
--

DROP TABLE IF EXISTS `emission_factor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emission_factor` (
  `activity` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `activity_full` mediumtext CHARACTER SET utf8 NOT NULL DEFAULT '',
  `unit` varchar(16) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `factor` double NOT NULL DEFAULT 0,
  `factor_ch4` double(22,0) DEFAULT 0,
  `factor_n2o` double(22,0) DEFAULT 0,
  `stage` tinyint(4) NOT NULL,
  `scope` tinyint(1) NOT NULL,
  PRIMARY KEY (`activity`),
  KEY `stage` (`stage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-29  0:34:57
