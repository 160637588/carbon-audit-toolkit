<?php
require_once '../form/header.php';
require_once '../form/function.php';

require __DIR__ . '/vendor/autoload.php';

use PHPJasper\PHPJasper;

$output = __DIR__ . '/output/'. $_SESSION['audit_id'].'/' ;

$section_end = array(2,3,4);
$section_name = array('construction', 'demolition', 'operation');
$stage = explode(', ', $_SESSION['building_stage']);


if(!file_exists($output)) {

	foreach($section_end as $section){

		if(in_array($section_name[$section-2], $stage)) {
			if(!file_exists($output)) {
				mkdir($output, 0777);
			}
			if(file_exists($output.'section'.$section.'.pdf')){
				unlink($output.'section'.$section.'.pdf');
			}
			$options = [
			    'format' => ['pdf'],
			    'locale' => 'en',
			    'params' => [
					'audit_id' => $_SESSION['audit_id']
				],
			    'db_connection' => [
			        'driver' => 'mysql',
			        'username' => 'ghg_php',
			        'password' => 'ra2iG68Eb8BiVeRE62QOt6MIqA454E',
			        'host' => '127.0.0.1',
			        'database' => 'ghg',
			        'port' => '3306'
			    ]
			];

			$jasper = new PHPJasper;

			$input = __DIR__ . '/section'.$section.'.jasper';

			$jasper->process(
			    $input,
			    $output,
			    $options
			)->execute();
		}

	}
}


function array_replace_value(&$ar, $value, $replacement)
{
    if (($key = array_search($ar, $value)) !== FALSE) {
        $ar[$key] = $replacement;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.6.0"><!-- Required meta tags -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Bootstrap CSS -->
  <link rel="stylesheet" href="/form/res/bootstrap.min.css">
  <link href="/form/res/main.css" rel="stylesheet">
  <link href="/form/res/chosen/component-chosen.min.css" rel="stylesheet">
  <title>Report</title>
</head>
<body>
  <nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="#">Carbon Auditing Toolkit</a>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item">Section 1</li>
        <li class="breadcrumb-item">Section 2</li>
        <li class="breadcrumb-item">Section 3</li>
        <li class="breadcrumb-item active">Report</li>
      </ol>
    </nav>
  </nav>
  <main role="main" class="container">
    <div class="jumbotron">
      <h1>Report for "<?php echo $_SESSION['building_name'];?>" is ready</h1>
      <div class="container">
		<?php if(in_array('construction', $stage) ) { ?><p><a href="output/<?php echo $_SESSION['audit_id'].'/section2.pdf';?>">Please click here to download the pdf report for construction phase.</a></p><?php } ?>
		<?php if(in_array('demolition', $stage) ) { ?><p><a href="output/<?php echo $_SESSION['audit_id'].'/section3.pdf';?>">Please click here to download the pdf report for demolition phase.</a></p><?php } ?>
		<?php if(in_array('operation', $stage) ) { ?><p><a href="output/<?php echo $_SESSION['audit_id'].'/section4.pdf';?>">Please click here to download the pdf report for operation phase.</a></p><?php } ?>
      </div>
    </div>
  </main><!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="/form/res/jquery-3.4.1.slim.min.js"></script>
  <script src="/form/res/popper.min.js"></script>
  <script src="/form/res/bootstrap.min.js"></script>

  <script src="/form/res/chosen/chosen.jquery.min.js"></script>
  <script src="/form/res/chosen/init.js"></script>
</body>
</html>
