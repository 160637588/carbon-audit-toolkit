<?php


require __DIR__ . '/vendor/autoload.php';

use PHPJasper\PHPJasper;

$input = __DIR__ . '/section2.jrxml';

$jasper = new PHPJasper;
$jasper->compile($input)->execute();
