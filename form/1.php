<?php
require_once 'header.php';
require_once 'function.php';

if (isset($_POST['submit']) && $_POST['submit']==1) {
    switch (@$_POST['user_type']) {
                case "end_user":
                        $_SESSION['user_type'] = $_POST['user_type'];
                        redirect('1-2-2_end-user.php');
                case "contractor":
                        $_SESSION['user_type'] = $_POST['user_type'];
                        redirect('1-1-1_contractor.php');
                case "organization":
                        $_SESSION['user_type'] = $_POST['user_type'];
                        redirect('1-1-2_other.php');
                case "other":
                        $_SESSION['user_type'] = $_POST['user_type'];
                        redirect('1-1-2_other.php');
                default:
                        $GLOBALS['err'][0] = 1; // Error occur
                        $GLOBALS['err_msg'][0] = 'Please select one option.';
        }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.6.0"><!-- Required meta tags -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Bootstrap CSS -->
  <link rel="stylesheet" href="res/bootstrap.min.css">
  <link href="res/main.css" rel="stylesheet">
  <title>Landing Page</title>
	<style>.gitlab-corner-wrapper{overflow:hidden;width:100px;height:100px;position:absolute;top:0;right:0}.gitlab-corner{position:absolute;top:-16px;right:-50px;transform:rotate(45deg);background:#333;border:44px solid #333;border-bottom:none;border-top:#333 solid 16px}.gitlab-corner svg{width:60px;height:60px;margin-bottom:-4px}.cls-1{fill:#fc6d26}.cls-2{fill:#e24329}.cls-3{fill:#fca326}.gitlab-corner:hover .cls-1{animation:cycle .6s}.gitlab-corner:hover .cls-2{animation:cycleMid .6s}.gitlab-corner:hover .cls-3{animation:cycleEnd .6s}@keyframes cycle{100%,15%,60%{fill:#fc6d26}30%,75%{fill:#e24329}45%,90%{fill:#fca326}}@keyframes cycleMid{100%,15%,60%{fill:#e24329}30%,75%{fill:#fca326}45%,90%{fill:#fc6d26}}@keyframes cycleEnd{100%,15%,60%{fill:#fca326}30%,75%{fill:#fc6d26}45%,90%{fill:#e24329}}@media (max-width:500px){.gitlab-corner:hover .cls-1,.gitlab-corner:hover .cls-2,.gitlab-corner:hover .cls-3{animation:none}.gitlab-corner .cls-1{animation:cycle .6s}.gitlab-corner .cls-2{animation:cycleMid .6s}.gitlab-corner .cls-3{animation:cycleEnd .6s}}</style>
</head>
<body>
  <nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #e3f2fd;">
	<div class="gitlab-corner-wrapper"><a href="https://gitlab.com/160637588/carbon-audit-toolkit" class="gitlab-corner" aria-label="View source on GitLab"><svg id="logo_art" data-name="logo art" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 586 559"><g id="g44"><path id="path46" class="cls-1" d="M461.17,301.83l-18.91-58.12L404.84,128.43a6.47,6.47,0,0,0-12.27,0L355.15,243.64H230.82L193.4,128.43a6.46,6.46,0,0,0-12.26,0L143.78,243.64l-18.91,58.19a12.88,12.88,0,0,0,4.66,14.39L293,435,456.44,316.22a12.9,12.9,0,0,0,4.73-14.39"></path></g><g id="g48"><path id="path50" class="cls-2" d="M293,434.91h0l62.16-191.28H230.87L293,434.91Z"></path></g><g id="g56"><path id="path58" class="cls-1" d="M293,434.91,230.82,243.63h-87L293,434.91Z"></path></g><g id="g64"><path id="path66" class="cls-3" d="M143.75,243.69h0l-18.91,58.12a12.88,12.88,0,0,0,4.66,14.39L293,435,143.75,243.69Z"></path></g><g id="g72"><path id="path74" class="cls-2" d="M143.78,243.69h87.11L193.4,128.49a6.47,6.47,0,0,0-12.27,0l-37.35,115.2Z"></path></g><g id="g76"><path id="path78" class="cls-1" d="M293,434.91l62.16-191.28H442.3L293,434.91Z"></path></g><g id="g80"><path id="path82" class="cls-3" d="M442.24,243.69h0l18.91,58.12a12.85,12.85,0,0,1-4.66,14.39L293,434.91l149.2-191.22Z"></path></g><g id="g84"><path id="path86" class="cls-2" d="M442.28,243.69h-87.1l37.42-115.2a6.46,6.46,0,0,1,12.26,0l37.42,115.2Z"></path></g></svg></a></div>
    <a class="navbar-brand" href="#">Carbon Auditing Toolkit</a>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Section 1</li>
        <li class="breadcrumb-item">
          <a href="#">Section 2</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#">Section 3</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#">Report</a>
        </li>
      </ol>
    </nav>
  </nav>
  <main role="main" class="container">
    <div class="jumbotron">
      <h1>Section 1</h1>
      <p class="lead">Please answer the following question(s). This will determine the physical boundary of this carbon auditing.</p>
      <p class="lead">I am ...</p>
      <div class="container">
        <form action="" method="post">
          <ul class="list-group <?php is_invalid();?>">
            <li class="list-group-item custom-control custom-radio"><input type="radio" id="end_user" name="user_type" value="end_user" class="custom-control-input <?php is_invalid();?>"> <label class="custom-control-label" for="end_user">My store/house/unit only (End-user), e.g. Store owner/tentant</label></li>
            <li class="list-group-item custom-control custom-radio"><input type="radio" id="organization" name="user_type" value="organization" class="custom-control-input <?php is_invalid();?>"> <label class="custom-control-label" for="organization">I represent an organization</label></li>
            <li class="list-group-item custom-control custom-radio"><input type="radio" id="contractor" name="user_type" value="contractor" class="custom-control-input <?php is_invalid();?>"> <label class="custom-control-label" for="contractor">I am a contractor of construction and/or demolition</label></li>
            <li class="list-group-item custom-control custom-radio"><input type="radio" id="other" name="user_type" value="other" class="custom-control-input <?php is_invalid();?>"> <label class="custom-control-label" for="other">Other</label></li>
          </ul><?php form_error_msg();?><button type="submit" name="submit" value="1" class="btn btn-primary">Continue »</button>
        </form>
      </div>
    </div>
  </main><!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="res/jquery-3.4.1.slim.min.js"></script>
  <script src="res/popper.min.js"></script>
  <script src="res/bootstrap.min.js"></script>
</body>
</html>

