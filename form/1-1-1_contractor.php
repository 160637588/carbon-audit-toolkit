<?php
require_once 'header.php';
require_once 'function.php';

if (isset($_POST['submit']) && $_POST['submit']==1) {
    if (isset($_POST['building_stage']) && count($_POST['building_stage'])==1) {
		$c_type = $_POST['building_stage'][0];
        switch ($c_type) {
                    case "construction":
                        $_SESSION['building_stage'] = $c_type;
                        redirect('1-2-1_other.php');
                    case "demolition":
                        $_SESSION['building_stage'] = $c_type;
                        redirect('1-2-1_other.php');
                    default:
                        $GLOBALS['err'][0] = 1; // Error occur
                        $GLOBALS['err_msg'][0] = 'Unexpected error.';
            }
    } elseif (isset($_POST['building_stage']) && count($_POST['building_stage'])>1) {
      natsort($_POST['building_stage']);
      $_SESSION['building_stage'] = implode(', ', $_POST['building_stage']);
		  redirect('1-2-1_other.php');
    } else {
        $GLOBALS['err'][0] = 1; // Error occur
        $GLOBALS['err_msg'][0] = 'Please select at least one option.';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.6.0"><!-- Required meta tags -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Bootstrap CSS -->
  <link rel="stylesheet" href="res/bootstrap.min.css">
  <link href="res/main.css" rel="stylesheet">
  <title>Landing Page</title>
</head>
<body>
  <nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="#">Carbon Auditing Toolkit</a>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Section 1</li>
        <li class="breadcrumb-item">
          <a href="#">Section 2</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#">Section 3</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#">Report (phpjasper)</a>
        </li>
      </ol>
    </nav>
  </nav>
  <main role="main" class="container">
    <div class="jumbotron">
      <h1>Section 1</h1>
      <p class="lead">Please answer the following question(s). This will determine the scope of this carbon auditing.</p>
      <div class="container">
        <form action="" method="post">
          <ul class="list-group <?php is_invalid();?>">
            <li class="list-group-item custom-control custom-checkbox"><input type="checkbox" class="custom-control-input <?php is_invalid();?>" id="construction" name="building_stage[]" value="construction">
  					<label class="custom-control-label" for="construction">Construction Phase</label></li>
            <li class="list-group-item custom-control custom-checkbox"><input type="checkbox" class="custom-control-input <?php is_invalid();?>" id="demolition" name="building_stage[]" value="demolition">
  					<label class="custom-control-label" for="demolition">Demolition Phase</label></li>
          </ul>
			<?php form_error_msg();?><button type="submit" name="submit" value="1" class="btn btn-primary">Continue »</button>
        </form>
      </div>
    </div>
  </main><!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="res/jquery-3.4.1.slim.min.js"></script>
  <script src="res/popper.min.js"></script>
  <script src="res/bootstrap.min.js"></script>
</body>
</html>
