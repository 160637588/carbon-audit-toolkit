<?php
require_once 'header.php';
function redirect($url, $http_code = 303)
{
   header('Location: ' . $url, true, $http_code);
   die();
}

function is_invalid($id = 0)
{
	if(isset($GLOBALS['err'][$id]) && $GLOBALS['err'][$id] == 1){
		echo 'is-invalid';
	}
}

function form_error_msg($id = 0)
{
	if(isset($GLOBALS['err_msg'][$id])){
		echo '<div class="invalid-feedback">'.$GLOBALS['err_msg'][$id].'</div>';
	}
}

function form_process_text($expected_post)
{
	$post_array = array_map('trim', explode(",", $expected_post));
	if(is_array($post_array)){
		foreach($post_array as $post_name) {
			if (!isset($_POST[$post_name]) or strlen($_POST[$post_name])<1){
				$GLOBALS['err'][$post_name] = 1; // Error occur
				$GLOBALS['err_msg'][$post_name] = 'Please fill in this box.';
				$return = false;
				//return false;
			}
			else {
				$_SESSION[$post_name] = $_POST[$post_name];
				$return = true;
				//return true;
			}
		}
	}
	else {
		if (!isset($_POST[$post_array]) or strlen($_POST[$post_array])<1){
			$GLOBALS['err'][$post_array] = 1; // Error occur
			$GLOBALS['err_msg'][$post_array] = 'Please fill in this box.';
			$return = false;
			//return false;
		}
		else {
			$_SESSION[$post_array] = $_POST[$post_array];
			$return = true;
			//return true;
		}
	}

	return $return;
}

function mysql_ghg()
{
	require_once ('include/PHP-MySQLi-Database-Class/MysqliDb.php');
	require ('config/config.php');

	$db = new MysqliDb (Array (
                'host' => $config_mysql_host,
                'username' => $config_mysql_user,
                'password' => $config_mysql_password,
                'db'=> $config_mysql_database,
                'port' => 3306,
                'charset' => 'utf8'));
	return $db;
}

function list_emission_factor($stage = 0)
{
	$db = mysql_ghg();
	$db->where('stage', $stage);
	$res = $db->get('emission_factor');
	$_SESSION['emission_factor'][$stage] = $res;

  // Space is not allowed in HTML form name,
  // urlencode was used; abandoned due to bug discovered
  // base64 is used instead

	foreach($res as $res_item){
		echo '<option value="'.base64_encode($res_item['activity']).'">'.$res_item['activity_full'].'</option>'."\n";
	}
}

function create_activity_form($stage)
{
	if(!isset($_SESSION['emission_factor'][$stage])){
		echo 'An fatal error occur!';
		die();
	}
	if(isset($_SESSION['activity_select'][$stage])){
  	foreach($_SESSION['emission_factor'][$stage] as $ef_item){

  		if(in_array($act_encoded = base64_encode($ef_item['activity']), $_SESSION['activity_select'][$stage])){
  			echo <<<EOT
  				<label for="{$act_encoded}" class="col-sm col-form-label">{$ef_item['activity_full']} (in {$ef_item['unit']})</label>
  				<div class="col-sm-4">
  				  <input type="number" step=any class="form-control" id="{$act_encoded}" name="{$act_encoded}">
  				</div>
  EOT;
  		}

  	}
  }
}

function section_end($section)
{
  if(!is_int($section)) {
    die('section_end error: $section failed');
  }
	$db = mysql_ghg();


	foreach( $_POST as $stuff => $val ) {
    //var_dump($_POST);
     if( is_array( $stuff ) ) {
         foreach( $stuff as $thing) {
             //var_dump($thing);
         }
     } else {
		 if($stuff != 'submit' && $stuff != ''){
			// $stuff -> post_name, $val -> $val
			// $stuff == activity, $val == quantity
      $stuff = base64_decode($stuff);
      //var_dump($stuff);
			$subq_factor = $db->subQuery ();
			$subq_factor->where ("activity", $stuff);
			$subq_factor->getOne ("emission_factor", "factor");

			$subq_unit = $db->subQuery ();
			$subq_unit->where ("activity", $stuff);
			$subq_unit->getOne ("emission_factor", "unit");

			$subq_full = $db->subQuery ();
			$subq_full->where ("activity", $stuff);
			$subq_full->getOne ("emission_factor", "activity_full");

			$data = Array ("audit_id" => $_SESSION['audit_id'],
               "activity" => $stuff,
               "quantity" => $val,
			   "unit" => $subq_unit,
			   "factor" => $subq_factor
			);
			$id = $db->insert ('section'.$section, $data);
			if(!$id){
				echo 'Database error <br>';
				var_dump($db->getLastError());
				var_dump($data);
				die();
			}
		 }
     }
	}
}

?>
