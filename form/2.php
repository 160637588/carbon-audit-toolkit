<?php
require_once 'header.php';
require_once 'function.php';

$stage_array = explode(', ', $_SESSION['building_stage']);
if (!in_array('construction',$stage_array)){
	redirect('/form/3.php');
}


if (isset($_POST['submit']) && $_POST['submit']==1) {
	if (isset($_POST['activity_select_0']) || isset($_POST['activity_select_1'])){
		$_SESSION['activity_select'][0] = $_POST['activity_select_0'];
		$_SESSION['activity_select'][1] = $_POST['activity_select_1'];
		redirect('/form/2.1.php');
	}
	else{
		//echo 'Error';
		//die();
	}
	//var_dump ($_POST);

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.6.0"><!-- Required meta tags -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Bootstrap CSS -->
  <link rel="stylesheet" href="res/bootstrap.min.css">
  <link href="res/main.css" rel="stylesheet">
  <link href="res/chosen/component-chosen.min.css" rel="stylesheet">
  <title>Section 2</title>
</head>
<body>
  <nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="#">Carbon Auditing Toolkit</a>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item">Section 1</li>
        <li class="breadcrumb-item active">Section 2</li>
        <li class="breadcrumb-item">
          <a href="#">Section 3</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#">Report</a>
        </li>
      </ol>
    </nav>
  </nav>
  <main role="main" class="container">
    <div class="jumbotron">
      <h1>Section 2 - Construction Phase</h1>
      <p class="lead">Please select the information you can provided.</p>
      <div class="container">
        <form action="" method="post">
		<div>
			<label for="activity_select_0[]">General activity</label>
			<select name="activity_select_0[]" class="form-control form-control-chosen" multiple data-placeholder="Click here, and you can type to search">
			<?php list_emission_factor(0);?>
			</select>

			<label for="activity_select_1[]">Material used</label>
			<select name="activity_select_1[]" class="form-control form-control-chosen" multiple data-placeholder="Click here, and you can type to search">
			<?php list_emission_factor(1);?>
			</select>
		</div>
		  <button type="submit" name="submit" value="1" class="btn btn-primary">Continue »</button>
        </form>
      </div>
    </div>
  </main><!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="res/jquery-3.4.1.slim.min.js"></script>
  <script src="res/popper.min.js"></script>
  <script src="res/bootstrap.min.js"></script>

  <script src="res/chosen/chosen.jquery.min.js"></script>
  <script src="res/chosen/init.js"></script>
</body>
</html>
