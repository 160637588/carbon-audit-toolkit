<?php
require_once 'header.php';
require_once 'function.php';

if (isset($_POST['submit']) && $_POST['submit']==1) {
	$form_processed = form_process_text('building_name,building_location,property_name');
	if($form_processed){
		$db = mysql_ghg();
		$data = Array (
		   "building_name" => $_POST['building_name'],
		   "building_location" => $_POST['building_location'],
		   "property_name" => $_POST['property_name'],
		   "building_stage" => 'operation'
		);

		$id = $db->insert ('section1', $data);

		if($id){
			$_SESSION['audit_id'] = $id;
			$_SESSION['building_stage'] = 'operation';
			redirect('2.php');
		}
		else{
			echo 'TERRIBLE ERROR! <br>';
			var_dump($id);
			var_dump($db->getLastError());
			die();
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.6.0"><!-- Required meta tags -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Bootstrap CSS -->
  <link rel="stylesheet" href="res/bootstrap.min.css">
  <link href="res/main.css" rel="stylesheet">
  <title>Section 1</title>
</head>
<body>
  <nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="#">Carbon Auditing Toolkit</a>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Section 1</li>
        <li class="breadcrumb-item">
          <a href="#">Section 2</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#">Section 3</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#">Report (phpjasper)</a>
        </li>
      </ol>
    </nav>
  </nav>
  <main role="main" class="container">
    <div class="jumbotron">
      <h1>Section 1</h1>
      <p class="lead">Please answer the following question(s). This will determine the scope of this carbon auditing.</p>
      <div class="container">
        <form method="post" action="">
          <div class="form-group <?php is_invalid('building_name');?>">
            <label for="building_name">What is the name of the building?</label> <input type="text" class="form-control <?php is_invalid('building_name');?>" id="building_name" name="building_name" placeholder="e.g. THEi Tsing Yi Campus" value="<?php echo @$_POST['building_name'];?>">
          </div>
		  <?php form_error_msg('building_name'); ?>
          <div class="form-group <?php is_invalid('building_location');?>">
            <label for="building_location">What is the location of the building?</label> <input type="text" class="form-control <?php is_invalid('building_location');?>" id="building_location" name="building_location" placeholder="e.g. 20A Tsing Yi Road, Tsing Yi Island, New Territories " value="<?php echo @$_POST['building_location'];?>">
          </div>
		  <?php form_error_msg('building_location'); ?>
          <div class="form-group <?php is_invalid('property_name');?>">
            <label for="property_name">What is the name / room no. / unit no. of your property?</label> <input type="text" class="form-control <?php is_invalid('property_name');?>" id="property_name" name="property_name" placeholder="e.g. Room LT1 " value="<?php echo @$_POST['property_name'];?>">
          </div>
		  <?php form_error_msg('property_name'); ?>
		  <button type="submit" name="submit" value="1" class="btn btn-primary">Continue »</button>
        </form>
      </div>
    </div>
  </main><!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="res/jquery-3.4.1.slim.min.js"></script>
  <script src="res/popper.min.js"></script>
  <script src="res/bootstrap.min.js"></script>
</body>
</html>
