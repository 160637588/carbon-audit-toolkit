<?php
require_once 'header.php';
require_once 'function.php';

if (isset($_POST['submit']) && $_POST['submit']==1) {
	section_end(3);
	redirect('4.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.6.0"><!-- Required meta tags -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Bootstrap CSS -->
  <link rel="stylesheet" href="res/bootstrap.min.css">
  <link href="res/main.css" rel="stylesheet">
  <link href="res/chosen/component-chosen.min.css" rel="stylesheet">
  <title>Section 3</title>
</head>
<body>
  <nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="#">Carbon Auditing Toolkit</a>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item">Section 1</li>
        <li class="breadcrumb-item">Section 2</li>
        <li class="breadcrumb-item active">Section 3</li>
        <li class="breadcrumb-item">
          <a href="#">Report</a>
        </li>
      </ol>
    </nav>
  </nav>
  <main role="main" class="container">
    <div class="jumbotron">
      <h1>Section 3 - Demolition Phase</h1>
      <p class="lead">Please provide the following data, empty value will be ignored.</p>
      <div class="container">
        <form action="" method="post">
		  <div class="form-group row">
<?php create_activity_form(0);?>
<?php //create_activity_form(2);?>
		  </div>
		  <button type="submit" name="submit" value="1" class="btn btn-primary">Continue »</button>
        </form>
      </div>
    </div>
  </main><!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="res/jquery-3.4.1.slim.min.js"></script>
  <script src="res/popper.min.js"></script>
  <script src="res/bootstrap.min.js"></script>

  <script src="res/chosen/chosen.jquery.min.js"></script>
  <script src="res/chosen/init.js"></script>
</body>
</html>
